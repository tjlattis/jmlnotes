#! /usr/bin/env python3
# parseJMLNotes 1.1 - parse JML's notes to xlsx doc
# Author: Tony
# change log: 1.1 stndardizes date format to UN standard
# Usage: ./parseJMLNotes <input filename> <output filename>

import openpyxl, sys, re, argparse
from openpyxl.styles import Alignment
from datetime import datetime

start_time = datetime.now()

parser = argparse.ArgumentParser()
parser.add_argument("target", action="store", help="specify the target file") 
parser.add_argument("file_name", action="store", help="specify a name for the output file")

args = parser.parse_args()

itr = 0

# # check shell args
# if len(sys.argv) != 3:
#     print('Usage: ./parseJMLNotes <input filename> <output filename>')
#     sys.exit()

# Format output sheet
output = openpyxl.Workbook()
outSheet = output.active
outSheet['A1'] = 'Box'
outSheet.column_dimensions['A'].width = 20
outSheet['B1'] = 'Folder'
outSheet.column_dimensions['B'].width = 25
outSheet['C1'] = 'Title'
outSheet.column_dimensions['C'].width = 5
outSheet['D1'] = 'Party'
outSheet.column_dimensions['D'].width = 20
outSheet['E1'] = 'Date'
outSheet.column_dimensions['E'].width = 15
outSheet['F1'] = 'Notes'
outSheet.column_dimensions['F'].width = 40
outSheet['G1'] = 'src file'
outSheet.column_dimensions['G'].width = 10
output.save(args.file_name)

# this fuction reformats dates to yyyy.mm.dd; collapsed from parseDate.py
def parseDate(string):

    if string.strip() == '':
        return 'No Date'
    dateRegex = re.compile(r'(\d+)\s(\w+)\s(\d\d\d\d)(.*)')
    res = dateRegex.search(string)
    if res is None:
        return string
    monthString = res.group(2)
    monthString = monthString.lower()
    month = ""
    if monthString == 'jan' or monthString == 'january':
        month = '01'
    elif monthString == 'feb' or monthString == 'february':
        month = '02'
    elif monthString == 'mar' or monthString == 'march':
        month = '03'
    elif monthString == 'apr' or monthString == 'april':
        month = '04'
    elif monthString == 'may':
        month = '05'
    elif monthString == 'jun' or monthString == 'june':
        month = '06'
    elif monthString == 'jul' or monthString == 'july':
        month = '07'
    elif monthString == 'aug' or monthString == 'august':
        month = '08'
    elif monthString == 'sep' or monthString == 'september':
        month = '09'
    elif monthString == 'oct' or monthString == 'october':
        month = '10'
    elif monthString == 'nov' or monthString == 'november':
        month = '11'
    elif monthString == 'dec' or monthString == 'december':
        month = '12'
    date = str(res.group(3)) + '.' + month + '.' + str(res.group(1))
    text = str(res.group(4))
    field = date + text
    return field

# open input file
with open(args.target) as filestream:

    # Variables
    item = []
    boxValue = ""
    folderValue = ""
    partyValue = ""
    dateValue = ""
    isItem = False
    itemIt = 2

    # Loop over lines
    for line in filestream:

        # update box and folder numbers
        if isItem:
            item.append(line)
        if line.startswith('Box'):
            boxValue = line.strip()
        if line.startswith('FF'):
            folderValue = line[3:].strip()

        # begin item cycle
        if line.startswith('II'):
            if isItem:

                # Variables
                title = ""
                party = ""
                date = ""
                notes = ""

                # remove trailing 'II'
                item.pop()

                # iterate over item list and parse
                for i in item:
                    if str(i).startswith('II'):
                        title = i.strip()[3:]
                    elif str(i).startswith('PP'):
                        party = i[3:].strip()
                    elif str(i).startswith('DD'):
                        dateString = i[3:].strip()
                        date = parseDate(dateString)
                    elif str(i).startswith('Box'):
                        continue
                    elif str(i).startswith('FF'):
                        continue
                    elif str(i).startswith('>>'):
                        continue
                    elif str(i).startswith('End'):
                        continue
                    else:
                        notes = notes + i.strip()

                # write to output
                outSheet['A' + str(itemIt)] = boxValue
                outSheet['B' + str(itemIt)] = folderValue
                outSheet['C' + str(itemIt)] = title
                outSheet['D' + str(itemIt)] = party
                outSheet['E' + str(itemIt)] = date
                outSheet['F' + str(itemIt)] = notes
                outSheet['G' + str(itemIt)] = args.target
                output.save(str(sys.argv[2]))
                itr = itr + 1
                
                # tick iterator and clear item list
                itemIt += 1
                item = []
                
            isItem = True
            item.append(line.strip())

run_time = datetime.now() - start_time
print("processing finished")
print("%s items processed in %s" % (itr, run_time))

        